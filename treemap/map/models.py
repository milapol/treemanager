from django.contrib.gis.db import models


class Property(models.Model):
    name = models.CharField(max_length=24)


class Node(models.Model):
    name = models.CharField(max_length=40)
    coordinates = models.PointField()
    properties = models.ManyToManyField(Property)

    @property
    def latlon(self):
        return [self.geom[0], self.geom[1]]


class Value(models.Model):
    property = models.ForeignKey(Property(), on_delete=models.CASCADE)

    class Meta:
        abstract = True
