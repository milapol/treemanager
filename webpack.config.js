const path = require('path');

module.exports = {
  entry: '/treemap/assets/index.js',  // path to our input file
  output: {
    filename: 'map.js',  // output bundle file name
    path: path.resolve(__dirname, './treemap/map/static/'),  // path to our Django static directory
  },
};